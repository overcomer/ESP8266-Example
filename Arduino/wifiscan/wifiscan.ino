#include <ESP8266WiFi.h>

void setup() {
    Serial.begin(115200);
    Serial.println();
    int n = WiFi.scanNetworks();
    for (int i = 0; i < n; i++) {
        String result = WiFi.SSID(i) + ", 信道：" + WiFi.channel(i)
        + ", 信号强度：" + WiFi.RSSI(i) + "dBm";
        Serial.println(result);
    }
}

void loop() {
  // put your main code here, to run repeatedly:

}
