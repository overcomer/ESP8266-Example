#include <DHT.h>

DHT sensor(D1, DHT11);

void setup() {
    Serial.begin(115200);
    sensor.begin();
}

void loop() {
    float temp = sensor.readTemperature();
    float humi = sensor.readHumidity();
    String s = String(temp, 1) + "," + String(humi, 0);
    Serial.println(s);
    delay(1000);
}
