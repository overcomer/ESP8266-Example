#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>
#include <IRremoteESP8266.h>
#include <IRsend.h>

#define ARRAY_SIZE(a) (sizeof(a)/sizeof(a[0]))

WiFiClient client;
PubSubClient mqtt(client);

IRsend remote(D2);
//AUX AC
uint16_t poweron[211]={9085,4538,574,1697,578,1695,579,565,579,563,582,558,588,542,605,1671,611,1673,605,1693,582,1693,583,1689,587,1682,597,1668,608,547,600,1669,612,558,586,562,580,563,579,565,580,561,584,560,585,1671,606,1674,609,1671,605,563,581,562,581,563,582,559,581,558,589,542,605,543,605,561,586,562,578,565,580,563,580,562,583,558,585,1673,608,540,607,561,586,562,579,564,579,565,579,562,582,560,585,543,605,542,606,563,583,564,579,563,578,565,579,562,583,558,587,1673,609,539,609,559,585,564,579,563,581,562,580,562,585,556,590,541,604,543,607,558,586,565,577,565,579,564,579,563,585,555,588,541,607,539,608,560,586,562,581,562,582,561,581,560,585,556,590,1669,610,538,612,557,588,561,581,562,579,563,582,560,586,555,588,543,607,536,611,560,583,1695,582,562,579,1687,589,558,586,554,591,541,607,537,611,561,585,1691,583,1689,587,1686,589,559,586,554,590,1666,613,1666,614,563,581};
uint16_t poweroff[211]={9094,4526,587,1688,587,1687,589,555,589,555,589,555,591,538,611,1667,615,1666,614,1686,588,1685,590,1686,590,1683,594,1665,614,539,609,1665,616,554,590,556,588,555,587,557,588,557,590,554,588,1668,615,1664,616,1665,613,555,587,557,587,556,588,556,591,552,593,535,613,536,612,558,589,556,587,556,587,557,588,557,588,554,593,1666,613,535,613,554,591,557,585,558,586,557,588,555,591,553,593,534,612,537,612,556,589,557,586,557,588,556,587,556,589,556,589,1667,613,537,612,556,590,556,586,557,587,556,590,553,591,554,591,537,610,538,613,556,588,558,588,556,586,557,589,555,589,555,592,534,613,535,614,555,589,557,586,558,586,558,587,557,589,553,594,534,614,535,614,554,591,555,588,555,586,558,589,555,590,554,592,533,612,536,614,555,589,1687,590,554,586,1689,589,555,590,555,591,534,612,537,614,554,592,1686,586,1689,588,1686,590,556,589,554,592,536,612,1666,615,556,588};

void mqtt_callback(const char* topic, byte* payload, unsigned int length)
{
    const size_t capacity = JSON_OBJECT_SIZE(1) + 20;
    DynamicJsonDocument doc(capacity);

    deserializeJson(doc, payload, length);
    Serial.println(topic);

    const char* status = doc["status"];
    
    if (String(status) == "on")
    {
        Serial.println("ac on");
        remote.sendRaw(poweron, ARRAY_SIZE(poweron), 38);
    }
    
    if (String(status) == "off")
    {
        Serial.println("ac off");
        remote.sendRaw(poweroff, ARRAY_SIZE(poweroff), 38);
    }
}

void setup() {
    Serial.begin(115200);
    Serial.println();
    remote.begin();
    WiFi.mode(WIFI_STA);
    WiFi.begin("LIUYU", "12345678");
    while (!WiFi.isConnected())
    {
        delay(100);
    }
    Serial.println(WiFi.localIP());

    pinMode(A0, INPUT);

    mqtt.setServer("iot.tinytaro.cn", 1883);
    mqtt.setCallback(mqtt_callback);
}

void loop() {
    if (!mqtt.connected()) {
        const char* clientID = String(ESP.getChipId(), HEX).c_str();
        mqtt.connect(clientID, "hqyj", "opensesame", "hqyj/nodemcu/status", 0, 1, "offline");
        mqtt.publish("hqyj/nodemcu/status", "online", true);
        mqtt.subscribe("hqyj/ac");
    }
    mqtt.loop();
    const size_t capacity = JSON_OBJECT_SIZE(1);
    DynamicJsonDocument doc(capacity);
    doc["voltage"] = (analogRead(A0) * 3.3) / 1024;//需要先将整数转为浮点数，再做除法
    String msg;
    serializeJson(doc, msg);
    mqtt.publish("hqyj/voltage", msg.c_str());
    delay(1000);
}
