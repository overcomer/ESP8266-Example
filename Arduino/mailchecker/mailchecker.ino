/*
 * 每分钟检查一次QQ邮箱，如果有新邮件则点亮LED灯。
 */

#include <ESP8266WiFi.h>

WiFiClient mail;

int checkmail()
{
  mail.connect("pop.qq.com", 110);
  Serial.print(mail.readString());
  mail.print("user QQ号\r\n");
  Serial.print(mail.readString());
  mail.print("pass 授权码\r\n");
  Serial.print(mail.readString());
  mail.print("stat\r\n");
  String stat = mail.readString();
  int b1 = stat.indexOf(' ');
  int b2 = stat.indexOf(' ', b1 + 1);
  Serial.print(stat);
  mail.print("quit\r\n");
  return stat.substring(b1 + 1, b2).toInt();
}

void setup() {
  WiFi.mode(WIFI_STA);
  WiFi.begin("LIUYU", "12345678");
  Serial.begin(74880);
  Serial.println();
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);
  while(!WiFi.isConnected()) {
    delay(100);
  }
  Serial.println("WiFi is connected");
  Serial.println("IP: " + WiFi.localIP().toString());
}

void loop() {
  if (checkmail())
  {
    digitalWrite(LED_BUILTIN, LOW);
  }
  else
  {
    digitalWrite(LED_BUILTIN, HIGH);
  }
  delay(1000*60);
}
