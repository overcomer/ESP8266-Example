#include <SPI.h>
#include <MFRC522.h>

//refer https://github.com/miguelbalboa/rfid
//Signal  Pin
//RST     D3(0)
//SS/SDA  D8(15)
//MOSI    D7(13)
//MISO    D6(12)
//SCK     D5(14)

#define SS_PIN D8
#define RST_PIN D3

MFRC522 reader(SS_PIN, RST_PIN);

void setup() {
  Serial.begin(74880);
  while (!Serial);
  Serial.println();
  SPI.begin();
  reader.PCD_Init();
  reader.PCD_DumpVersionToSerial();
}

void loop() {
  if (!reader.PICC_IsNewCardPresent())
  {
    return;
  }
  if (!reader.PICC_ReadCardSerial())
  {
    return;
  }
  reader.PICC_DumpToSerial(&(reader.uid));
}
